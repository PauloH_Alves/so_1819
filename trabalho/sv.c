#include "sv.h"
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>


#define line_size 200
int senha = 0;

char erro_init [] = "Ficheiro STOCKS não existe ou não pode ser aberto, inicializando um novo STOCKS\n" ;

int verifica_stocks(int op)
{
    int descritor;
    if (op == 0)
    {

        if ((descritor = open("STOCKS", O_RDONLY)) == -1)
        {
            return -1;
        }
        else return descritor;
    }
    if (op == 1){

    }
    if (op == 2){

    }
}
int conta_linhas_artigos(int descritor){
    int i,bytes_lidos,linhas=0;
    char buffer[1024];
    while((bytes_lidos = read(descritor,buffer,1024))){

        for(i = 0 ; i < bytes_lidos ; i++){

            if(buffer[i] == '\n') {

                linhas++;
            }
        }
    }
    return linhas;
}
//inicializar com stocks a 0(op 1 ) ou ler do ficheiro INIT_STOCKS.txt os stocks(op 2)
int init_stocks(int op){
    char buffer_linha[23],buffer_init_stocks[10],buffer_geral[1024];
    int i,j=0,contador = 0,tamanho_escrita=0,bytes_lidos;
    int descritor_init ; //para abrir o INIT_STOCKS.txt
    int descritor_a = open("ARTIGOS", O_RDONLY);
    if(descritor_a == -1 ) return -1;
    int descritor_s = open("STOCKS",O_CREAT | O_RDWR,0666);
    if(op == 1){

        contador = conta_linhas_artigos(descritor_a);
        for ( i = 0 ; i< contador ; i++){
            tamanho_escrita = sprintf(buffer_linha,"%d 0\n",i);
            write(descritor_s,buffer_linha,tamanho_escrita);

        }
        close(descritor_a);
        return descritor_s;
    }
    else {
        descritor_init = open("INIT_STOCKS.txt",O_RDONLY);
        while((bytes_lidos = read(descritor_init,buffer_geral,1024))){
            for(i = 0 ; i<bytes_lidos ; i++){
                printf("bytes lidos %d\n",bytes_lidos);
                buffer_init_stocks[j] = buffer_geral[i];
                printf("%c = %c\n",buffer_init_stocks[j],buffer_geral[i] );
                j++;
                if(buffer_geral[i] == '\n'){
                    tamanho_escrita = snprintf(buffer_linha,j,"%d %s",contador,buffer_init_stocks);
                    printf("sprintf tamanho%d e %s",tamanho_escrita,buffer_linha);
                    write(descritor_s,buffer_linha,tamanho_escrita);
                    j=0;contador++;
                }
            }

        }

    }

}



int main(int argc, char const *argv[])
{
    int descritor;


    // opção default, ficheiro STOCKS tem que existir e é mantido, caso contrario é feita a inicialização com OP = 1
    if (argc == 1){
        if((descritor = verifica_stocks(0)) == -1){
            write(1,erro_init,sizeof(erro_init));
            descritor = init_stocks(1);
            if(descritor == -1 ){
                write(1,"ARTIGOS não existe ou não pode ser aberto, servidor a encerrar\n",sizeof("ARTIGOS não existe ou não pode ser aberto, servidor a encerrar\n"));
                close(descritor);
                return 0;
            }
            else{
                write(1,"STOCKS criado com sucesso\n",sizeof("STOCKS criado com sucesso\n"));
            }
        }
        else{
            write(1,"STOCKS aberto com sucesso\n",sizeof("STOCKS aberto com sucesso\n"));

        }
    }
    else {
        if(argv[1][0] == '1'){
            descritor = init_stocks(1);
            if(descritor == -1 ){
                write(1,"ARTIGOS não existe ou não pode ser aberto, servidor a encerrar\n",sizeof("ARTIGOS não existe ou não pode ser aberto, servidor a encerrar\n"));
                close(descritor);
                return 0;
            }
            else{
                write(1,"STOCKS criado com sucesso\n",sizeof("STOCKS criado com sucesso\n"));

                write(1,"**SERVIDOR**\n > status: online\n",sizeof("**SERVIDOR**\n > status: online\n"));
                write(1," > Pronto para receber pedidos..\n\n",sizeof(" > Pronto para receber pedidos..\n\n"));

                int fd, nbytes;
                char buffer[line_size];
                char cBuffer[line_size];
                int nClientes = 0;
                int i = 0;

                //escrita para ficheiro logs de execuções
                if((fd = open("logs.txt", O_CREAT | O_WRONLY | O_APPEND, 0666)) == -1){
                  perror("logs:ERROR\n");
                  return -1;
                }

                int fds, fdc, fdpedidos, pid, tamanho_senha;
                char nova_conexao[20];
                char senha_pipe[10];
                char pedido[50];
                int fds_clientes[10000];

                mkfifo("novos_clientes", 0666);
                mkfifo("senhas_enviadas", 0666);
                mkfifo("pedidos", 0666);

                fds = open("senhas_enviadas", O_WRONLY | O_NONBLOCK);
                fdc = open("novos_clientes", O_RDONLY | O_NONBLOCK);
                fdpedidos = open("pedidos", O_RDONLY | O_NONBLOCK);
                int fds_pipes_pessoais[10000];

                for(;;){
                  if(read(fdc, nova_conexao, 20) > 0){
                    if(nova_conexao[0] == 'n'){
                      tamanho_senha = sprintf(senha_pipe, "%d.p", senha);
                      senha++;
                      mkfifo(senha_pipe, 0666);
                      fds_clientes[senha-1] = open(senha_pipe, O_WRONLY | O_NONBLOCK);
                      write(fds, senha_pipe, tamanho_senha);
                    }
                    if(read(fds_clientes[senha-1], buffer, 4) > 0){
                      printf("buffer: %s\n", buffer);
                    }

                  }
                }

                close(fds);
                close(fdc);
                close(fdpedidos);
                close(fd);

              }
        }
        else{
            descritor = init_stocks(2);
        }
    }
    close(descritor);

    return 1;
}
