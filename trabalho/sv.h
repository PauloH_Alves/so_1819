#ifndef SV_H
#define SV_H

//usado para guardar strings com codigo ' ' , stock e '\n' 10 para cada valor no maximo mais os dois char e '\0'

char buffer_stock[23];

//decisão, o ficheiro STOCKS é sempre criado posteriormente ao de ARTIGOS e inicializado a 0 os stocks
//de todos os artigos presentes no Artigos(OP = 1) , inicializado lendo um ficheiro chamado INIT_STOCKS.txt (OP = 2)
// que contem uma lista de quantidades a escrever sequencialmente poara cada codigo e por ultimo pode manter
// o ficheiro que já existe (OP = DEFAULT main() sem argumentos)


//dado um codigo existente, muda o stock
int set_stock(int codigo, int stock);

//dado um codigo que exista de um artigo, devolve o stock existente
int get_stock(int codigo);


#endif