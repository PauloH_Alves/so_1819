#include "cv.h"
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>


#define LINE_SIZE 100
//argumentos passados ao stdin pelo utilizador, no maximo 3 argumentos
char argumentos[3][128];

//separa argumentos por aspas, similar a um strtoken
int separa_aspas(char* buffer,int n_argumento,int pos){
  int j=0,i;
  for(i = 0 ; buffer[pos+i] != 34; i++){

    argumentos[n_argumento][j] = buffer[pos+i] ;
    j++;
  }
  i++;
  return i;
}

//cria array de argumentos
int cria_argumentos(){
  memset(argumentos[0],0,sizeof(argumentos[0]));
  memset(argumentos[1],0,sizeof(argumentos[1]));
  memset(argumentos[2],0,sizeof(argumentos[2]));
  char buffer_stdin[256];
  int i=0,linha=0,coluna=0;
  while(read(0,buffer_stdin,256)){
    while(buffer_stdin[i]!='\n'){

      if(buffer_stdin[i] == 34 ){

        i++;
        if((i+=separa_aspas(buffer_stdin,linha,i)) < 2 ) return -1; //argumento vazio ou invalido

        coluna=i;
      }
      if(buffer_stdin[i] != 32 && buffer_stdin[i] != 34 && buffer_stdin[i] !='\n') {

        argumentos[linha][coluna] = buffer_stdin[i] ;
        i++;coluna++;

      }
      else {

        argumentos[linha][coluna] = '\0';
        linha++;coluna=0;
        if(buffer_stdin[i]!='\n') i++;
      }

    }
    argumentos[linha][coluna] = '\0';
    return linha+1;
  }

}

int info_artigo(unsigned int codigo){
/*  int fd;
  int pid;

  if((fd = open(novastr, O_WRONLY)) == -1) {
		perror(">[ERROR: CLIENT_OPEN_PIPE!]\n");
		return -1;
	}
	else{
    if((pid = fork()) == 0){
      char buffer[LINE_SIZE];
      sprintf(buffer, "info %d", codigo);
      write(fd, buffer, sizeof(buffer));
      _exit(0);
    }
    else{
      close(fd);
      waitpid(pid, NULL, 0);
      return 1;
    }
  }*/
}

int operacao(unsigned int codigo, int quantidade){
/*  int fd;
  int pid;

  if((fd = open(novastr, O_WRONLY)) == -1) {
		perror(">[ERROR: CLIENT_OPEN_PIPE!]\n");
		return -1;
	}
	else{
    if((pid = fork()) == 0){
      char buffer[LINE_SIZE] = " ";
      sprintf(buffer, "inserir %d %d", codigo, quantidade);
      write(fd, buffer, sizeof(buffer));
      _exit(0);
    }
    else{
      close(fd);
      waitpid(pid, NULL, 0);
      return 1;
    }
  } */
}

/*
int gerafifo() {
	char *validchars = "abcdefghijklmnopqrstuvwxyz";
  //novastr = NULL;
	int i;
	int str_len;

	// inicia o contador aleatório
	srand(time(NULL ));

	// novo tamanho
	str_len = (rand() % MAX_STR_SIZE) + 1;


	// aloca memoria
	novastr = (char*) malloc((str_len + 1) * sizeof(char));

	if ( !novastr ){
		//printf("[*] Erro ao alocar memoria.\n" );
		_exit(1);
	}

	// gera string aleatória
	for ( i = 0; i < str_len; i++ ) {
		novastr[i] = validchars[ rand() % strlen(validchars) ];
		novastr[i + 1] = 0x0;
	}

	// check resultado
  char pname[30];
  sprintf(pname, ">[PipeName]: '%s'\n", novastr);
	write(1, pname, 19+str_len);

	return 0;
}
*/
int main(int argc, char const *argv[]) {

  int fd, pid;
  char ops[250];
  int c_fifo;
  char buffer[LINE_SIZE];
  int pname_fifo;
  int fd_cliente;

    if ((pid = fork()) == 0){

          int fds, fdc, fdpedidos, pid, tamanho_senha;
          int i,flag = 1,senha_pessoal;

          char senha[10];
          fdc = open("novos_clientes", O_WRONLY | O_NONBLOCK);
          fds = open("senhas_enviadas", O_RDONLY | O_NONBLOCK);
          fdpedidos = open("pedidos", O_WRONLY | O_NONBLOCK);
          write(fdc,"n",1);

          if(read(fds, senha, 10) > 0){
            senha_pessoal = strtol(senha, NULL, 10);
            //printf("senhas: %s\n", senha);
            fd_cliente = open(senha, O_RDONLY | O_NONBLOCK);
            write(fd_cliente, "OLA\n", sizeof("OLA\n"));
          }

          int flagcv=1;
          while(flagcv){
            write(1,
            "\n\t\t\t**********COMANDOS**********\n\t\t\t***************************\n\n\t\tinfo <codigo>\t\t\t\t[informação sobre um artigo]\n\t\tinserir <codigo> <quantidade>\t\t[insere um artigo em STOCKS]\n\t\tsair\n",
            sizeof("\n\t\t\t**********COMANDOS**********\n\t\t\t***************************\n\n\t\tinfo <codigo>\t\t\t\t[informação sobre um artigo]\n\t\tinserir <codigo> <quantidade>\t\t[insere um artigo em STOCKS]\n\t\tsair\n"));

            write(1, ">", sizeof(">"));
            cria_argumentos();
            if(strcmp(argumentos[0],"sair") == 0)
              flagcv=0;
            if(strcmp(argumentos[0],"info") == 0)
              info_artigo(strtol(argumentos[1],NULL,10));
            if(strcmp(argumentos[0],"inserir") == 0)
              operacao(strtol(argumentos[1],NULL,10),strtol(argumentos[2],NULL,10));
            }



        close(fd_cliente);
        _exit(0);
    }
    else{
      waitpid(pid, NULL, 0);
      return 0;
    }

}
