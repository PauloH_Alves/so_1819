#ifndef CV_H
#define CV_H

//pede ao servidor de vendas informação sobre stock e preço de um artigo existente
int info_artigo(unsigned int codigo);


//pede ao servidor de vendas que altere o stock de um artigo existente(que gera uma venda/atualização de stock)
int operacao(unsigned int codigo, int quantidade);


#endif
