#include "ma.h"
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 1024*1024*1024

//buffer de texto para UI
char ops[2048];
//argumentos passados ao stdin pelo utilizador, no maximo 3 argumentos
char argumentos[3][128];

//auxiliar para contar linhas do ficheiro ARTIGOS
unsigned int conta_linhas()
{
    char buffer_auxiliar[1024];
    int bytes_lidos;
    unsigned int contador = 0;
    int descritor = open("ARTIGOS", O_RDONLY);
    if (descritor == -1)
        return 0;

    while (bytes_lidos = read(descritor, buffer_auxiliar, sizeof(buffer_auxiliar)))
    {
        for (int i = 0; i < bytes_lidos; i++)
        {
            if (buffer_auxiliar[i] == '\n')
            {
                contador++;
            }
        }
    }
    return contador;
}
//separa argumentos por aspas, similar a um strtoken
int separa_aspas(char* buffer,int n_argumento,int pos){
  int j=0,i;
  for(i = 0 ; buffer[pos+i] != 34; i++){

    argumentos[n_argumento][j] = buffer[pos+i] ;
    j++;
  }
  i++;
  return i;
}

//cria array de argumentos
int cria_argumentos(){
  memset(argumentos[0],0,sizeof(argumentos[0]));
  memset(argumentos[1],0,sizeof(argumentos[1]));
  memset(argumentos[2],0,sizeof(argumentos[2]));
  char buffer_stdin[256];
  int i=0,linha=0,coluna=0;
  while(read(0,buffer_stdin,256)){
    while(buffer_stdin[i]!='\n'){

      if(buffer_stdin[i] == 34 ){

        i++;
        if((i+=separa_aspas(buffer_stdin,linha,i)) < 2 ) return -1; //argumento vazio ou invalido

        coluna=i;
      }
      if(buffer_stdin[i] != 32 && buffer_stdin[i] != 34 && buffer_stdin[i] !='\n') {

        argumentos[linha][coluna] = buffer_stdin[i] ;
        i++;coluna++;

      }
      else {

        argumentos[linha][coluna] = '\0';
        linha++;coluna=0;
        if(buffer_stdin[i]!='\n') i++;
      }

    }
    argumentos[linha][coluna] = '\0';
    return linha+1;
  }

}

//semelhante ao lseek mas por linhas
int posiciona_offset(int descritor, int linha)
{
    char buffer_auxiliar[1024];
    int bytes_lidos;
    int offset;
    unsigned int contador = 0;
    while (bytes_lidos = read(descritor, buffer_auxiliar, sizeof(buffer_auxiliar)))
    {
        for (int i = 0; i < bytes_lidos; i++)
        {
            if (buffer_auxiliar[i] == '\n')
            {
                offset = offset + i;
                contador++;
                if (contador == linha)
                {
                    return offset;
                }
            }
        }
    }
}

int i(char *nome, float preco)
{

    //abre o ficheiro ARTIGOS e posiciona-se no final do ficheiro(O_APPEND)
    int descritor = open("ARTIGOS", O_WRONLY | O_APPEND);
    int fds = open("STRINGS", O_WRONLY | O_APPEND);

    //converter e copiar para o buffer o novo codigo e preço
    //separados por espaço e terminados com '\n'
    int tamanho_linha = sprintf(buffer_linha, "%u %g\n", proximo_artigo, preco);
    int pointerS = sprintf(buffer_nomes, "%s\n", nome);

    //escrever no ficheiro ARTIGOS o buffer e actualizar o proximo_artigo
    write(descritor, buffer_linha, tamanho_linha);
    write(fds, buffer_nomes, pointerS);
    proximo_artigo++;
    close(descritor);
    return 1;
}

//alterar nome de um artigo se existir
int n(int codigo, char *novo_nome){
  int pid;

  if((pid = fork()) == 0){
    char buffer[32];
    int fd = open("ARTIGOS", O_RDONLY);

    sprintf(buffer, "s/^%d .*/%s/", codigo, novo_nome);

    //comando para dar replace (filho)
    execlp("sed", "sed", "-i", buffer, "STRINGS", NULL);

    close(fd);
    _exit(1);
  }
  else{
    waitpid(pid, NULL, 0);
    return 1;
  }
}

int p(int codigo, float novo_preco)
{

    int pid;

    //trabalho delegado ao filho, o pai simplesmente espera a execução
    if ((pid = fork()) == 0)
    {

        //buffer para a expressão regular
        char regex[32];

        //forma a expressão regular com o codigo e novo preço
        sprintf(regex, "s/^%d .*/%d %g/", codigo, codigo, novo_preco);

        //executa o comando para dar replace (delegado a um filho)
        execlp("sed", "sed", "-i", regex, "ARTIGOS", NULL);
        _exit(1);
    }
    else
    {
        waitpid(pid, NULL, 0);
        return 1;
    }
}

//altera o preço de um artigo com uma percentagem(desconto)
int pd(int codigo, float desconto){
  float novo_preco = 0;
  char buffer[200];
  int fd = open("ARTIGOS", O_RDONLY);
  int readResult;
  int pid;
  int i = 0;

  if((pid = fork()) == 0){

    while((i < codigo) && ((readResult = read(fd, buffer, 200)) > 0))
      i++;

    char* token = strtok(buffer, " ");
    int nt = 1;
    if(i == codigo){
      while (i<=2) {
        token = strtok(NULL, " ");
        i++;
      }
      novo_preco = (atoi(token)) - (atoi(token) * (desconto/100));
    }

    char printS[200] = " ";
    sprintf(printS, "s/^%d .*/%d %g/", codigo, codigo, novo_preco);
    execlp("sed", "sed", "-i", printS, "ARTIGOS", NULL);
  }
  else{
    waitpid(pid, NULL, 0);
    close(fd);
    return 1;
  }
  //write(1, printS, sizeof(printS));
}

//altera o preço de todos os artigos baseado num desconto
int l(float desconto){
  //printf("VOU ENTRAR NO PTD : %g\n", desconto);
  float novo_preco = 0;
    char buffer[BUF_SIZE];
    int buf_size = BUF_SIZE;
    int fd = open("ARTIGOS", O_RDWR);
    printf("%d\n", fd);
    ssize_t readResult = 0;
    int pid;
    ssize_t nbytes = 0;

    if(fd == -1){
      printf("ERRO!\n");
      return -1;
    }

    char regex[32];
    int nt=0; //codigo
    int nl=0;
    float preco = 0;
    char* token = NULL;
    char *linha = NULL;
    char precoLinha[10] = " ";
    int cod = 0;

    while ((readResult = read(fd, buffer, buf_size)) > 0){
      printf("estou no ciclo\n");
      while (nl < readResult) {
        if(buffer[nl] == ' '){
          precoLinha[nt] = buffer[nl];
          nl++;
          nt++;
          if(buffer[nl] == '\n'){
            nt=0;
            preco = strtof(precoLinha,NULL);
            printf("-> %g\n", preco);
          }
        }
        nl++;
      }
    }

    close(fd);
    return 1;
  }

  //mostra a quantidade de artigos existentes
  int qa(){
    char printS[100] = " ";
    sprintf(printS, "Total de artigos: %d\n", proximo_artigo);
    write(1, printS, sizeof(printS));
    return proximo_artigo;
  }

//mostra qual o artigo mais caro existente
int amc(){
  float max = 0;
  int maxInd = 0;
  char buffer[200];
  int fd = open("ARTIGOS", O_RDONLY);
  int fds = open("STRINGS", O_RDONLY);
  int readResult;
  int pid;
  char printS[100] = " ";

  if((pid = fork())==0){
    float preco = 0;
    char* token = NULL;
    char* codigo = NULL;
    int nt;
    while ((readResult = read(fd, buffer, 200)) > 0) {
      token = strtok(buffer, " ");
      codigo = strdup(token);
      nt = 1;
      while (nt<2) {
        token = strtok(NULL, " ");
        preco = atof(token);
        nt++;
      }
      if(preco > max){
        max = preco;
        maxInd = atoi(codigo);
      }
    }


    int i = 0;
    char bufferS[128] = " ";

    while (i<maxInd && ((readResult = read(fds, bufferS, 128)) > 0))
      i++;

    sprintf(printS, "Artigo mais caro: %d - %s\n", maxInd, bufferS);
    write(1, printS, sizeof(printS));
  }
  else{
    waitpid(pid, NULL, 0);
    close(fd);
    close(fds);
    return 1;
  }
}

//mostra o artigo mais barato existente
int amb(){
  float min = 1;
  int minInd = 0;
  char buffer[200];
  int fd = open("ARTIGOS", O_RDONLY);
  int fds = open("STRINGS", O_RDONLY);
  int readResult;
  int pid;

  if((pid = fork())==0){
    float preco = 0;
    char* token = NULL;
    char* codigo = NULL;
    int nt;
    while ((readResult = read(fd, buffer, 200)) > 0) {
      token = strtok(buffer, " ");
      codigo = strdup(token);
      nt = 1;
      while (nt<2) {
        token = strtok(NULL, " ");
        preco = atof(token);
        nt++;
      }
      if(min > preco){
        min = preco;
        minInd = atoi(codigo);
      }
    }

    int i = 0;
    char bufferS[128] = " ";

    while (i<minInd && ((readResult = read(fds, bufferS, 128)) > 0))
      i++;

    char printS[100] = " ";
    sprintf(printS, "Artigo mais barato: %d - %s\n", minInd, bufferS);

    write(1, printS, sizeof(printS));
  }
  else{
    waitpid(pid, NULL, 0);
    close(fd);
    close(fds);
    return 1;
  }
}

//calcula e mostra a média de preço dos artigos
int mp(){
  float media = 0;
  float soma = 0;
  int contador = 0;
  char buffer[200];
  int fd = open("ARTIGOS", O_RDONLY);
  int readResult;
  int nbytes = 0;

  char* token = NULL;
  int nt;

  while ((nbytes = read(fd, buffer, sizeof(buffer))) > 0) {
          nt = 1;
          token = strtok(buffer, " ");
          while (nt<2) {
            token = strtok(NULL, " ");
            soma += atof(token);
            nt++;
          }
          contador++;
  }


  media = soma / contador;
  char printS[100] = " ";
  sprintf(printS, "Média de preços: %g €\n", media);
  write(1, printS, sizeof(printS));
  close(fd);
  return 1;
}

int man(char* funcao){
  return 1;
}
//na execução do programa é sempre feita a contagem de
//linhas do ficheiro ARTIGOS para atualizar a variavel global
//proximo_artigo
int main()
{

    int descritorArtigos, descritorStrings, pid;
    char op;
    char comando[100];

    if ((descritorArtigos = open("ARTIGOS", O_CREAT | O_APPEND | O_RDWR, 0666)) == -1)
    {

        write(1, "ARTIGOS ainda não existe\n", sizeof("ARTIGOS ainda não existe\n"));
        //close(descritorArtigos);
    }

    if((descritorStrings = open("STRINGS", O_CREAT | O_APPEND | O_RDWR, 0666)) == -1){
      write(1, "STRINGS ainda não existe\n", sizeof("STRINGS ainda não existe\n"));
    }

    //preencher buffer de texto para UI
    sprintf(ops, "\n\t\t\t**********FUNÇÕES**********\n\t\t\
    \t***************************\n\n\t\t\
    i <nome> <preço>\n\t\t\
    n <codigo> <novo nome>\n\t\t\
    p <codigo> <novo preço>\n\t\t\
    pd <codigo> <desconto>\n\t\t\
    ptd <desconto>\n\t\t\
    qa\n\t\t\
    amc\n\t\t\
    amb\n\t\t\
    mp\n\t\t\
    man <funcao> abre o manual\n\t\t\
    sair terminar\n");

    //criar filho e iniciar ciclo

    if ((pid = fork()) == 0)
    {

      //mostrar comandos disponíveis

      int flag=1;
      while(flag){
        write(1, ops, sizeof(ops));
        write(1, ">", sizeof(">"));
        cria_argumentos();
        if(strcmp(argumentos[0],"sair") == 0) flag=0;
        if(strcmp(argumentos[0],"i") == 0) i(argumentos[1],strtof(argumentos[2],NULL));
        if(strcmp(argumentos[0],"n") == 0) n(strtol(argumentos[1],NULL,10),argumentos[2]);
        if(strcmp(argumentos[0],"p") == 0) p(strtol(argumentos[1],NULL,10),strtof(argumentos[2],NULL));
        if(strcmp(argumentos[0],"pd") == 0) pd(strtol(argumentos[1],NULL,10),strtof(argumentos[2],NULL));
        if(strcmp(argumentos[0],"ptd") == 0){
          //printf("desconto = %s\n", argumentos[1]);
          l(strtof(argumentos[1],NULL));
        }
        if(strcmp(argumentos[0],"qa") == 0) qa();
        if(strcmp(argumentos[0],"amc") == 0) amc();
        if(strcmp(argumentos[0],"amb") == 0) amb();
        if(strcmp(argumentos[0],"mp") == 0) mp();
        if(strcmp(argumentos[0],"man") == 0) man(argumentos[1]);

      }
    }
    else
    {
        waitpid(pid, NULL, 0);
        return 1;
    }
}

// int main(){
//   cria_argumentos();
//   printf("arg0 %s arg1 %s arg2 %s\n",argumentos[0],argumentos[1],argumentos[2]);
// }
