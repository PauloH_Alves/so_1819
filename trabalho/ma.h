#ifndef MA_H
#define MA_H

//unsigned int 4,294,967,295 ou seja 10 caracteres para representar
//espaço entre codigo e linha + \n e \0 3 caracteres
//999,999,999.99 preço maximo ou seja mais 12 caracteres a contar com o '.'
//total de 25 caracteres no máximo por linha aceite no ficheiro ARTIGOS
char buffer_linha[25];
char buffer_nomes[100];
//auxiliar para saber o proximo codigo a criar
unsigned int proximo_artigo=0;

//manutenção de artigos, permite aceder a um ficheiro chamado ARTIGOS e
//inserir ou modificar os artigos presentes
//o ficheiro artigos contem codigos e preços e os respectivos nomes
//estão num ficheiro chamado STRINGS


//inserir novo artigo com nome e preço e mostra o codigo com qual foi criado
int i(char* nome, float preco);

//alterar nome de um artigo se existir
int n(int codigo, char* novo_nome);

//alterar preço de um artigo
int p(int codigo, float novo_preco);


//altera o preço de um artigo com uma percentagem(desconto)
int pd(int codigo, float desconto);

//altera o preço de todos os artigos baseado num desconto
int ptd(float desconto);

//mostra a quantidade de artigos existentes
int qa();


//mostra qual o artigo mais caro existente
int amc();

//mostra o artigo mais barato existente
int amb();

//calcula e mostra a média de preço dos artigos
int mp();

#endif
