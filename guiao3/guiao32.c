#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>


int main(){
    int id;
    if((id=fork()) == 0 ){
        if(execlp("ls","ls","-l",NULL) == -1) write(2,"erro no exec\n",14);
        _exit(0);
    }
    wait(NULL);
    return 0;
}