#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
char buffer [256];

int main(){

    pid_t id;
    if ((id = fork()) == 0) {
      printf("id proprio: %d    e id do pai : %d\n",getpid(),getppid());
      return 0;
    }
    else  { 
      wait(NULL);
      printf("id proprio : %d   e id do filho :do %d\n",getpid(),id);
      return 0;
      }

}

