#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>

int main(){
    int status;
    pid_t ids[10];
    for(int i = 0; i < 10; i++)  
    {
        ids[i]=fork();
        if(ids[i]==0){printf("Filho de id %d e ppid %d\n",getpid(),getppid());_exit(i);}
        
    }
    for(int i=0 ; i<10 ; i++){
        waitpid(ids[i],&status,0);
        if(WIFEXITED(status)){
            printf("Filho com codigo %d terminou com codigo de saida %d\n",ids[i],WEXITSTATUS(status));

        }
    }
    return 0;
    
   
    
    

}