#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
char buffer [256];

int main(){
    pid_t id;
    for(int i = 1; i < 11; i++)  
    {
        id=fork();
        if(id==0){

        printf("Sou filho de id = %d e o id do meu pai = %d\n",getpid(),getppid());
        _exit(i);

        }
         else{
            waitpid(id,NULL,0);
             printf("Filho numero %d terminou e tinha pid = %d\n",i,id);
            
        }
    }
    return 0;
    
   
    
    

}