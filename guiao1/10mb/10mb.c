#include <unistd.h>
#include <fcntl.h> //O_RDONLY, O_WRONLY, O_CREAT, O_RDWR, O_TRUNC

int main(int argc, char* argv[]){
	if(argc!=2) return-1;
	int fdopen= open(argv[1],O_CREAT|O_RDWR|O_TRUNC);
	for(int i=0;i<10000000;i++){
		write(fdopen,"a",1);
	}
	return 0;
}
