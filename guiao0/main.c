#include <stdio.h>
#include "guiao0.h"

int main(){

	Person andre = new_person("Andre",20);
	printf("nome : %s\nidade : %d\n", andre.nome,andre.idade);
	person_change_age(&andre,24);
	printf("A nova idade do %s é %d\n", andre.nome,andre.idade);
	Person nova_pessoa=clone_person(&andre);
	printf("Clone = nome : %s\nidade : %d\n", nova_pessoa.nome,nova_pessoa.idade);
	
	destroy(&nova_pessoa);
	destroy(&andre);


return 0;
}
