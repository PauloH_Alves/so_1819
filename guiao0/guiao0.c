#include "guiao0.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

Person new_person(char* nome,int idade){

	Person *pessoa=malloc(sizeof(Person));
	
	if(pessoa==NULL) exit(0);

		pessoa->nome=malloc((1+strlen(nome))*sizeof(char));
	
	if(pessoa->nome==NULL) {

		free(pessoa);

		exit(0);

	}

	strcpy(pessoa->nome,nome);

	pessoa->idade=idade;

	return (*pessoa);

}

/*Person* new_person(char* nome,int idade){

	Person *pessoa_ptr=malloc(sizeof(Person));
	
	if(pessoa==NULL) return NULL;

		pessoa->nome=malloc((1+strlen(nome))*sizeof(char));
	
	if(pessoa->nome==NULL) {

		free(pessoa_ptr);

		return NULL;

	}

	strcpy(pessoa->nome,nome);

	pessoa->idade=idade;

	return pessoa_ptr;

}
*/

Person clone_person(Person* p){

	if(p==NULL) exit(0);

	Person *novapessoa_ptr=malloc(sizeof(Person));
	novapessoa_ptr->nome=malloc((1+strlen(p->nome))*sizeof(char));
	strcpy(novapessoa_ptr->nome,p->nome);
	novapessoa_ptr->idade=p->idade;
	
	return *novapessoa_ptr;

}


void destroy(Person* p){

	free(p->nome);
	free(p);
}


int person_age(Person* p){

	return p->idade;

}
void person_change_age(Person*p, int idade){

	p->idade=idade;

}



